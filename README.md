# keyringctl

`keyringctl` manages a keyring daemon on the system. This provides a common 
interface to use add and retrieve passwords and SSH keys to the keyring. This 
can also be useful on systems without a display manager that would otherwise 
unlock the keyring automatically. Currently, only the Gnome keyring is 
supported as the underlying keyring.

# Requirements

`keyringctl` requires `secret-tool` which comes with
[`libsecret`](https://gitlab.gnome.org/GNOME/libsecret). `libsecret` should be 
installed from the system's repositories. `keyringctl` is written in Lua and
requires a Lua interpreter and some Lua packages. 

## Interpreter

Lua >= 5.2

## Packages

These can be installed using [LuaRocks](https://luarocks.org). They may 
also be present in the system's repositories.

- [argparse](https://github.com/mpeterv/argparse)
- [luaposix](https://github.com/luaposix/luaposix)

# Installation

The `keyringctl` script can be downloaded separately and copied to a directory 
that is in $PATH. 

# Usage

Run the following command on the terminal to see the list of commands and 
options available

```
keyringctl -h
```
